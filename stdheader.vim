" **************************************************************************** "
"                                                                              "
"                                                         :::      ::::::::    "
"    stdheader.vim                                      :+:      :+:    :+:    "
"                                                     +:+ +:+         +:+      "
"    By: jerry <marvin@42.fr>                       +#+  +:+       +#+         "
"                                                 +#+#+#+#+#+   +#+            "
"    Created: 2020/07/04 15:34:59 by jerry             #+#    #+#              "
"    Updated: 2020/07/04 15:36:00 by jerry            ###   ########.fr        "
"                                                                              "
" **************************************************************************** "

let s:asciiart = [
			\"        :::      ::::::::",
			\"      :+:      :+:    :+:",
			\"    +:+ +:+         +:+  ",
			\"  +#+  +:+       +#+     ",
			\"+#+#+#+#+#+   +#+        ",
			\"     #+#    #+#          ",
			\"    ###   ########.fr    "
			\]

let s:styles = [
			\{
			\'shebang': '',
			\'extensions': ['\.sh$', '\.shrc$'],
			\'start': '#', 'end': '#', 'fill': '*'
			\},
			\{
			\'shebang': '',
			\'extensions': ['\.c$', '\.h$', '\.cc$', '\.hh$', '\.cpp$', '\.hpp$'],
			\'start': '/*', 'end': '*/', 'fill': '*'
			\},
			\{
			\'shebang': '',
			\'extensions': ['\.htm$', '\.html$', '\.xml$'],
			\'start': '<!--', 'end': '-->', 'fill': '*'
			\},
			\{
			\'shebang': '',
			\'extensions': ['\.js$'],
			\'start': '//', 'end': '//', 'fill': '*'
			\},
			\{
			\'shebang': '',
			\'extensions': ['\.tex$'],
			\'start': '%', 'end': '%', 'fill': '*'
			\},
			\{
			\'shebang': '',
			\'extensions': ['\.ml$', '\.mli$', '\.mll$', '\.mly$'],
			\'start': '(*', 'end': '*)', 'fill': '*'
			\},
			\{
			\'shebang': '',
			\'extensions': ['\.vim$', 'vimrc$', '\.myvimrc$', 'vimrc$'],
			\'start': '"', 'end': '"', 'fill': '*'
			\},
			\{
			\'shebang': '',
			\'extensions': ['\.el$', '\.emacs$', '\.myemacs$'],
			\'start': ';', 'end': ';', 'fill': '*'
			\},
			\{
			\'shebang': '#!/usr/bin/env python3',
			\'extensions': ['\.py$', '\.py3$'],
			\'start': '#', 'end': '#', 'fill': '*'
			\},
			\{
			\'shebang': '',
			\'extensions': ['\.f90$', '\.f95$', '\.f03$', '\.f$', '\.for$'],
			\'start': '!', 'end': '!', 'fill': '/'
			\}
			\]

let s:linelen		= 80
let s:marginlen		= 5
let s:contentlen	= s:linelen - (3 * s:marginlen - 1) - strlen(s:asciiart[0])
let s:trimfile = strpart(fnamemodify(bufname('%'), ':t'), 0, s:contentlen)

function s:trimlogin ()
	let l:trimlogin = strpart($USER, 0, 9)
	if strlen(l:trimlogin) == 0
		let l:trimlogin = "marvin"
	endif
	return l:trimlogin
endfunction

function s:trimemail ()
	let l:trimemail = strpart($MAIL, 0, s:contentlen - 16)
	if strlen(l:trimemail) == 0
		let l:trimemail = "marvin@42.fr"
	endif
	return l:trimemail
endfunction

function s:midgap ()
	return repeat(' ', s:marginlen - 1)
endfunction

function s:lmargin ()
	return repeat(' ', s:marginlen - strlen(s:start))
endfunction

function s:rmargin ()
	return repeat(' ', s:marginlen - strlen(s:end))
endfunction

function s:empty_content ()
	return repeat(' ', s:contentlen)
endfunction

function s:left ()
	return s:start . s:lmargin()
endfunction

function s:right ()
	return s:rmargin() . s:end
endfunction

function s:bigline ()
	return s:start . ' ' . repeat(s:fill, s:linelen - 2 - strlen(s:start) - strlen(s:end)) . ' ' . s:end
endfunction

function s:logo1 ()
	return s:left() . s:empty_content() . s:midgap() . s:asciiart[0] . s:right()
endfunction

function s:fileline ()
	return s:left() . s:trimfile . repeat(' ', s:contentlen - strlen(s:trimfile)) . s:midgap() . s:asciiart[1] . s:right()
endfunction

function s:logo2 ()
	return s:left() . s:empty_content() . s:midgap() .s:asciiart[2] . s:right()
endfunction

function s:coderline ()
	let l:contentline = "By: ". s:trimlogin () . ' <' . s:trimemail () . '>'
	return s:left() . l:contentline . repeat(' ', s:contentlen - strlen(l:contentline)) . s:midgap() . s:asciiart[3] . s:right()
endfunction

function s:logo3 ()
	return s:left() . s:empty_content() . s:midgap() .s:asciiart[4] . s:right()
endfunction

function s:dateline (prefix, logo)
	let l:date = strftime("%Y/%m/%d %H:%M:%S")
	let l:contentline = a:prefix . ": " . l:date . " by " . s:trimlogin ()
	return s:left() . l:contentline . repeat(' ', s:contentlen - strlen(l:contentline)) . s:midgap() . s:asciiart[a:logo] . s:right()
endfunction

function s:createline ()
	return s:dateline("Created", 5)
endfunction

function s:updateline ()
	return s:dateline("Updated", 6)
endfunction

function s:emptyline ()
	return s:start . repeat(' ', s:linelen - strlen(s:start) - strlen(s:end)) . s:end
endfunction

function s:shebang ()
	return s:shebang . repeat(' ', s:linelen - strlen(s:shebang))
endfunction

function s:release ()
	let l:release = '0.0.0'
	return s:left() . 'Version: ' . l:release  . repeat(' ', s:linelen - strlen(s:left()) - strlen(s:end) - strlen('Version: ') - strlen(l:release)) . s:end
endfunction

function s:interpreter ()
	let l:interpreter = 'GNU bash, version 3.2.57(1)-release (x86_64-apple-darwin18)'
	return s:left() . 'Interpreter: ' . l:interpreter  . repeat(' ', s:linelen - strlen(s:left()) - strlen(s:end) - strlen('Interpreter: ') - strlen(l:interpreter)) . s:end
endfunction

function s:usage ()
	let l:usage = s:trimfile  . ' [-aDde] [-f | -g] [-n number] req1 req2 [opt1 [opt2]]'
	return s:left() . 'Usage: ' . l:usage  . repeat(' ', s:linelen - strlen(s:left()) - strlen(s:end) - strlen('Usage: ') - strlen(l:usage)) . s:end
endfunction

function s:filetype ()
	let l:file = fnamemodify(bufname("%"), ':t')

	let s:start = '#'
	let s:end = '#'
	let s:fill = '*'
	let s:shebang = ''

	for l:style in s:styles
		for l:ext in l:style['extensions']
			if l:file =~ l:ext
				let s:shebang = l:style['shebang']
				let s:start = l:style['start']
				let s:end = l:style['end']
				let s:fill = l:style['fill']
			endif
		endfor
	endfor
endfunction

function s:insert42 ()
	call s:filetype ()

	call append(0, "")
	call append (0, s:bigline())
"	if s:shebang != ''
"		call append (0, s:emptyline())
"		call append (0, s:release())
"		call append (0, s:usage())
"		call append (0, s:emptyline())
"		call append (0, s:interpreter())
"	endif
	call append (0, s:emptyline())
	call append (0, s:updateline())
	call append (0, s:createline())
	call append (0, s:logo3())
	call append (0, s:coderline())
	call append (0, s:logo2())
	call append (0, s:fileline())
	call append (0, s:logo1())
	call append (0, s:emptyline())
	call append (0, s:bigline())
	if s:shebang != ''
		call append (0, s:shebang())
	endif
endfunction

function s:update42 ()
	call s:filetype ()

	let l:pattern = s:start . repeat(' ', 5 - strlen(s:start)) . "Updated: [0-9]"

	if strlen(s:shebang()) != 0
		let l:line = getline (10)
		if l:line =~ l:pattern
			call setline(10, s:updateline())
		endif
	else
		let l:line = getline (9)
		if l:line =~ l:pattern
			call setline(9, s:updateline())
		endif	
	endif
endfunction

command Stdheader42 call s:insert42 ()
nmap <F2> :Stdheader42<CR>
autocmd BufWritePre * call s:update42 ()

