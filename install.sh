#!/usr/bin/env sh                                                               
# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    install.sh                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jerry <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/07/04 16:30:55 by jerry             #+#    #+#              #
#    Updated: 2020/07/04 16:33:19 by jerry            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

if [ "$UID" != 0 ]
then
  echo source "$(pwd)/$(dirname $0)/stdheader.vim" >> "$HOME/.vimrc"
else
  cp -v ./stdheader.vim /usr/share/vim/vim73/plugin/stdheader.vi || true
  cp -v ./stdheader.vim /usr/share/vim/vim74/plugin/stdheader.vi || true
  cp -v ./stdheader.vim /usr/share/vim/vim80/plugin/stdheader.vi || true
fi
